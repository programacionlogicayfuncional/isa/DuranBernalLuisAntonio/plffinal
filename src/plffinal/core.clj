(ns plffinal.core)

(defn regresa-al-punto-de-origen?
  [secuencia]
  (let [ci [:equis 10 :yes 10] ca [:equis 10 :yes 10]]
    (if (empty? secuencia)
      (if (= ca ci) "true" "false")
      (case (first secuencia)
        \< (regresa-al-punto-de-origen? (rest secuencia))
        \^ (regresa-al-punto-de-origen? (rest secuencia))
        \> (regresa-al-punto-de-origen? (rest secuencia))
        \v (regresa-al-punto-de-origen? (rest secuencia))
      )
      )))
   
(defn regresa-al-punto-de-origen?2
  [secuencia]
  (let [ci [10 10] ca [10 10]]
    (if (empty? secuencia)
      (if (= ca ci) "true" "false")
       (if (= (compare (first secuencia) \<) 0)
         (regresa-al-punto-de-origen?2 (rest secuencia))
         (if (= (compare (first secuencia) \>) 0)
           (regresa-al-punto-de-origen?2 (rest secuencia))
           (if (= (compare (first secuencia) \^) 0)
             (regresa-al-punto-de-origen?2 (rest secuencia))
             (if (= (compare (first secuencia) \v) 0)
               (regresa-al-punto-de-origen?2 (rest secuencia))
               (regresa-al-punto-de-origen?2 (rest secuencia))
             ))))))
) 
 
 
(defn regreso-al-punto-de-origen
  [secuencia]  
  (let [ci [10 10] ca [10 10] ]
   (if (empty? secuencia)
     (if (= ca ci) ()
       "no son iguales")
     ( case (first secuencia)  
       \< (cons \> (regreso-al-punto-de-origen (rest secuencia)))
       \^ (cons \v (regreso-al-punto-de-origen (rest secuencia)))
       \> (cons \< (regreso-al-punto-de-origen (rest secuencia)))
       \v (cons \^ (regreso-al-punto-de-origen (rest secuencia)))
     )
   )))


 
