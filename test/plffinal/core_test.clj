(ns plffinal.core-test
  (:require [clojure.test :refer :all]
            [plffinal.core :refer :all]))

(deftest sobre-regreso-al-punto-de-origen
  (testing "vacio"
    (is (= '() ((regreso-al-punto-de-origen ""))))
    (is (= '(\< \< \<) ((regreso-al-punto-de-origen ">>>")))
  ))
)
  